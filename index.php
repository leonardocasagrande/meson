<?php get_header(); ?> <section class="banner-home-meson"> <?php
  $images = get_field('imagem_desktop');
  if ($images) : ?> <div class="d-none d-lg-block"><div class="wrapper"><div class="carrosel-banner-home"> <?php foreach ($images as $image_id) : ?> <div class="item"><div class="b-home" style="background-image:url(<?= $image_id ?>);"><div class="bg-shadow"></div></div></div> <?php endforeach; ?> </div></div></div> <?php endif; ?> <?php
  $imagesMob = get_field('imagem_mobile');
  if ($imagesMob) : ?> <div class="d-lg-none"><div class="wrapper"><div class="carrosel-banner-home-mob"> <?php foreach ($imagesMob as $image_id_mob) : ?> <div class="item"><div class="b-home" style="background-image:url(<?= $image_id_mob ?>);"><div class="bg-shadow"></div></div></div> <?php endforeach; ?> </div></div></div> <?php endif; ?> </section><section class="ensinos"><div class="red mr-lg-2"><div class="container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/e-fundamental.png" alt=""> <span class="title">Ensino Fundamental</span><p>Compreende a fase do 1º ao 9º anos, atendendo desde crianças de 6 anos até adolescentes de 14 anos de idade. Por ser o período mais longo da educação básica, é dividido entre Fundamental I e Fundamental II.</p><a href="<?= get_site_url(); ?>/ensino-fundamental" class="btn-cta col-6">Conheça melhor</a></div></div><div class="blue ml-lg-2"><div class="container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/e-medio.png" alt=""> <span class="title">Ensino Médio</span><p>Com duração de 3 anos, seu principal objetivo é aprimorar os conhecimentos já obtidos e preparar os jovens para entrar no mercado de trabalho ou construir uma carreira de nível superior.</p><a href="<?= get_site_url(); ?>/ensino-medio" class="btn-cta col-6">Conheça melhor</a></div></div></section><section class="somos-anglo"><div class="testa"><span class="title pr-lg-2">Somos únicos.</span> <span class="title"><b>Somos Anglo.</b></span></div><div class="container grid"><div class="lg-grid-right col-lg-6 pl-lg-5 ml-lg-5 ml-0 px-0"><div class="line d-none d-lg-block col-3 px-0"></div><div><p>Só poderíamos aliar nosso know-how em educação ao sistema que é líder em aprovações nas melhores e mais prestigiadas universidades do Brasil. Com mais de 70 anos de tradição dedicados ao aprendizado que traz resultado, o Sistema Anglo de Ensino contribui para o desenvolvimento da autonomia dos alunos, desde a Educação Infantil até o Pré-Vestibular, tornando-os protagonistas de suas próprias trajetórias escolares.</p></div><a href="<?= get_site_url(); ?>/sistema-anglo" class="btn-cta d-lg-flex d-none col-lg-4 col-6">Conheça melhor</a></div><div class="foto-grid px-0 col-lg-6 pl-lg-3"><img class="img-anglo col-4 px-0 d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/anglo.png" alt=""> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/materiais-2-home.jpg" alt=""><!-- <img class="img-anglo col-4 px-0 d-lg-none mx-auto d-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/anglo.png" alt=""> --> <img class="img-anglo d-lg-block d-none col-3 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/anglo.png" alt=""></div><a href="" class="btn-cta d-lg-none col-6">Conheça melhor</a></div></section><!-- <section class="estrutura estrutura-home">

  <div class="head">

    <span class="title"><b>Nossa estrutura</b> sustenta futuros brilhantes</span>

    <div class="line d-lg-none"></div>

  </div> <?= get_template_part('carousel-estrutura'); ?> </section> --><section class="alunos-aprovados"><div class="testa"><span class="title">Excelência em <b>aprovação</b> nas melhores universidades</span><p>Ano a ano, reafirmamos o nosso compromisso com o futuro e a carreira de centenas de jovens. No próximo, um deles pode ser o seu filho ou a sua filha!</p></div><!-- AQUI VAI CARROSSEL ALUNOS --> <?php get_template_part('alunos', 'aprovados') ?> </section><section class="eventos"><div class="banner-blue"><span class="title">Mais que aprendizado, <b>experiências para toda a vida</b></span></div><!-- PARTE DE EVENTOS --><div class="wrapper-eventos-home p-0 container-lg"><div class="d-lg-none evento-home-mobile"> <?php
      wp_reset_query();
      $argsMobile = array(
        'post_type' => 'evento',
        'posts_per_page' => 1
      );
      $eventoMobile = new WP_Query($argsMobile);
      if ($eventoMobile->have_posts()) : while ($eventoMobile->have_posts()) : $eventoMobile->the_post();
      ?> <div class="img-destaque"> <?php the_post_thumbnail() ?> </div><div class="infos"><div class="data"><i class="far fa-calendar-alt"></i> <?= get_field('data') ?></div><h2><?php the_title(); ?></h2><p><?php the_excerpt(); ?></p><a href="<?php the_permalink(); ?>">Veja todas as fotos</a></div> <?php endwhile;
      endif; ?> </div><div class="d-none d-lg-block evento-home-desk"><div class="carosel-eventos"> <?php
        wp_reset_query();
        $argsDesktop = array(
          'post_type' => 'evento',
          'posts_per_page' => 3
        );
        $eventoDesktop = new WP_Query($argsDesktop);
        if ($eventoDesktop->have_posts()) : while ($eventoDesktop->have_posts()) : $eventoDesktop->the_post();
        ?> <div class="item"><div class="container"><div class="row"><div class="col-6 informacoes"><div class="data"><i class="far fa-calendar-alt"></i> <?= get_field('data') ?></div><h2><?php the_title(); ?></h2><p><?php the_excerpt(); ?></p><a href="<?php the_permalink(); ?>">Veja todas as fotos</a></div><div class="col-6"><div class="row h-100"><div class="col-6 align-self-start imagem-destaque px-2 pb-2 pt-0"><?php the_post_thumbnail() ?></div> <?php
                      $images = get_field('galeria_de_fotos');
                      if ($images) :
                        // var_dump($images);
                      ?> <div class="col-6 align-self-start galeria pt-0 pb-2 px-2"><div class="img-galeria"><img src="<?= $images[0] ?>" alt=""></div></div><div class="col-6 align-self-end pt-2 pb-0 px-2"><div class="img-galeria"><img src="<?= $images[1] ?>" alt=""></div></div><div class="col-6 align-self-end pt-2 pb-0 px-2"><div class="img-galeria"><img src="<?= $images[2] ?>" alt=""></div></div> <?php endif; ?> </div></div></div></div></div> <?php endwhile;
        endif; ?> </div></div><div class="text-center"><a class="btn-cta cta-alunos col-8 col-lg-2 px-0" href="<?= get_site_url() ?>/eventos">Ver todos os eventos</a></div></div></section> <?php get_footer(); ?>