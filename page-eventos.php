<?php get_header(); 
$paged = (isset($_GET['pagina']) ? $_GET['pagina'] : 1); 
?> <?php get_search_form() ?> <section class="wrapper-eventos p-0 container-fluid"><div class="wrapper-eventos-home p-0 container"><div class="evento-home-mobile"> <?php
      wp_reset_query();
      $argsMobile = array(
        'post_type' => 'evento',
        'posts_per_page' => 3,
        'orderby' => 'rand'
      );
      $eventoMobile = new WP_Query($argsMobile);
      if ($eventoMobile->have_posts()) : while ($eventoMobile->have_posts()) : $eventoMobile->the_post();
      ?> <div class="item-slide"><div class="row-lg"><div class="img-destaque col-lg-7 p-0 px-lg-3"> <?php the_post_thumbnail() ?> </div><div class="infos col-lg-5"><div class="data pb-3"><i class="far fa-calendar-alt"></i> <?= get_field('data') ?></div><h2 class="pb-3"><?php the_title(); ?></h2><p class=""><?php the_excerpt(); ?></p><a class="mt-3" href="<?php the_permalink(); ?>">Veja todas as fotos</a></div></div></div> <?php endwhile;
      endif; ?> </div></div></section><div class="list-eventos container wrapper-eventos-home"><div class="row evento-home-mobile" id="app-eventos"> <?php
    wp_reset_query();
    $argsListar = array(
      'post_type' => 'evento',
      'posts_per_page' => 9,
      'paged' => $paged,

    );
    $eventoListar = new WP_Query($argsListar);
    if ($eventoListar->have_posts()) : while ($eventoListar->have_posts()) : $eventoListar->the_post();
    ?> <div class="col-lg-4 evento-listado"><div class="imagem-destaque"> <?php the_post_thumbnail() ?> </div><div class="infos"><div class="data pb-3"><i class="far fa-calendar-alt"></i> <?= get_field('data') ?></div><h2 class="pb-3"><?php the_title(); ?></h2><a href="<?php the_permalink(); ?>">Veja todas as fotos</a></div></div> <?php endwhile;
    endif; ?> </div></div><div class="barradenavegacao"> <?php
  
   echo paginate_links(array(
       'format' =>
       '?pagina=%#%', 'show_all' => false, 'current' => max(1, $paged), 'total' => $eventoListar->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
       'type' => 'list'
   ));
   ?> </div> <?php get_footer() ?>