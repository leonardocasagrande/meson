$(".menu-icon").on("click", function () {
  $(".top-nav").toggleClass("ativo");
});

$("#accordion button").on("click", function () {
  $(".icon").removeClass("icon-up");

  var isOpen = $(this).attr("aria-expanded");
  let element = this.childNodes[2];

  element.classList.add("icon-up");

  if (isOpen === "true") {
    element.classList.remove("icon-up");
  }
});

$(document).on("scroll", function () {
  if ($(document).scrollTop() >= 140) {
    $("header").addClass("scrolled");
  } else {
    $("header").removeClass("scrolled");
  }
});

$("#get-evento").on("click", function () {
  var pagina = $("#get-evento").data("page");
  var busca = $("#get-evento").data("busca");
  getEventos(pagina, busca);
  pagina += 1;
  $("#get-evento").attr("data-page", pagina);
});
function getEventos(paged, busca) {
  // var categoria = $(this).data('categoria') ;
  const app = $("#app-eventos");
  $.ajax({
    url: `${site}/wp-json/wp/v2/evento/?&_embed`,
    type: "GET",
    data: {
      per_page: 9,

      search: busca,

      page: paged,
    },
    dataType: "json",
    statusCode: {
      200: function (data) {
        console.log(data);
        var content;
        if (data.length) {
          content = `
              ${data
                .map(
                  (element) => `
                 
                      <div class="col-lg-4 evento-listado">
                      <div class="imagem-destaque">
                        <img src="${element._embedded["wp:featuredmedia"]["0"].source_url}">
                      </div>
                      <div class="infos">
                            <div class="data"><i class="far fa-calendar-alt"></i> ${element.acf.data}</div>
                            <h2>${element.title.rendered}</h2>
                            <a href="${element.link}">Veja todas as fotos</a>
                        </div>
                    </div>
                  
                `
                )
                .join("")}`;
        }

        app.append(content);
      },
    },
  });
}

// //alunos
// console.log(site);
// console.log(stylesheet);

// $("#get-aluno").on("click", function () {
//   var pagina = $("#get-aluno").data("page");
//   var busca = $("#get-aluno").data("busca");
//   getalunos(pagina, busca);
//   pagina += 1;
//   $("#get-aluno").attr("data-page", pagina);
// });
// function getalunos(paged, busca) {
//   // var categoria = $(this).data('categoria') ;
//   const app = $("#app-alunos");
//   $.ajax({
//     url: `${site}/wp-json/wp/v2/aluno/?&_embed`,
//     type: "GET",
//     data: {
//       per_page: 8,

//       search: busca,

//       page: paged,
//     },
//     dataType: "json",
//     statusCode: {
//       200: function (data) {
//         console.log(data);
//         var content;
//         if (data.length) {
//           content = `
//               ${data
//                 .map(
//                   (element) => `

//                     <div class="col-md-6 col-lg-3 px-0 px-lg-3 alunos">
//                       <div class="aluno ">
//                         <div class="foto-aluno" style="background-image:url(${element._embedded["wp:featuredmedia"]["0"].source_url})">
//                           <img src="${stylesheet}/dist/img/mascara-aprovados.png" alt="">
//                         </div>
//                         <div class="info-aluno">
//                           <h3 class="nome">${element.title.rendered}</h3>
//                           <p class="curso">${element.acf.curso}</p>

//                         </div>
//                       </div>
//                     </div>
//                 `
//                 )
//                 .join("")}`;
//         }

//         app.append(content);
//       },
//     },
//   });
// }

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$(".e-mail").attr("onChange", "validateEmail(this)");

$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

$(function () {
  var sliderEstruturaHome = tns({
    container: ".carousel-etrutura-home",
    slideBy: 1,
    autoplay: false,
    loop: false,
    controls: false,
    autoplayButtonOutput: false,
    responsive: {
      200: {
        items: 1,
      },
      1280: {
        gutter: 0,
        items: 3,
        autoplay: true,
        controls: false,
        navPosition: "bottom",
        controlsText: [
          '<i class="fas fa-chevron-left"></i>',
          '<i class="fas fa-chevron-right"></i>',
        ],
      },
    },
  });
});

$(function () {
  var sliderEquipe = tns({
    container: ".carousel-equipe",
    slideBy: "page",
    autoplay: false,
    loop: false,
    controls: false,
    nav: false,
    autoplayButtonOutput: false,
    responsive: {
      200: {
        items: 1.3,
        gutter: 10,
      },
      768: {
        items: 2.2,
        gutter: 10,
      },
      1280: {
        gutter: 15,
        items: 5,
      },
    },
  });
});

$(function () {
  var sliderAluno = tns({
    container: ".carousel-alunos",
    items: 1,
    slideBy: "page",
    autoplay: true,
    loop: true,
    nav: true,
    navPosition: "bottom",
    controls: false,
    controlsPosition: "bottom",
    controlsText: [
      '<i class="fas fa-chevron-left"></i>',
      '<i class="fas fa-chevron-right"></i>',
    ],
    autoplayButtonOutput: false,
    gutter: 10,
    responsive: {
      640: {
        items: 2,
      },
      1024: {
        items: 3,
        nav: false,
        controls: true,
      },
      1366: {
        items: 4,
      },
    },
  });
});

$(function () {
  var sliderevento = tns({
    container: ".carosel-eventos",
    items: 1,
    slideBy: "page",
    autoplay: true,
    loop: true,
    controls: true,
    controlsPosition: "bottom",
    controlsText: [
      '<i class="fas fa-chevron-left"></i>',
      '<i class="fas fa-chevron-right"></i>',
    ],
    nav: false,
    autoplayButtonOutput: false,
  });
});

$(function () {
  var sliderBanner = tns({
    container: ".carrosel-banner-home",
    items: 1,
    slideBy: "page",
    autoplay: true,
    loop: true,
    controls: false,
    nav: true,
    navPosition: "bottom",
    autoplayButtonOutput: false,
  });
});

$(function () {
  var sliderBanner = tns({
    container: ".carrosel-banner-home-mob",
    items: 1,
    slideBy: "page",
    autoplay: true,
    loop: true,
    autoplayButtonOutput: false,
    controls: false,
    navPosition: "bottom",
  });
});
