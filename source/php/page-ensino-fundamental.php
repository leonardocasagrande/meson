<?= get_header(); ?>

<section class="texto-intro page-ensinos">
  <div class="container ">
    <div class="d-lg-flex pb-lg-5">
      <div class="texto">

        <span class="pre-title">Fundamental I - do 1º ao 5º ano</span>

        <span class="title px-3 px-lg-0">A hora e o lugar certos para <b>adquirir conhecimento e desenvolver a autonomia</b></span>

        <div class="line col-5"></div>

        <p>No Méson, a atividade educacional do Ensino Fundamental I é orientada de modo a que nossos alunos adquiram conhecimento e desenvolvam autonomia, por meio de práticas de leitura, escrita e raciocínio lógico, práticas somadas à compreensão dos ambientes natural, social e político, bem como ao contato com as artes e a tecnologia. Acreditamos que essas são habilidades imprescindíveis tanto para a boa convivência familiar e vida em sociedade quanto para o desenvolvimento escolar nos níveis seguintes.</p>

        <p>“Aula dada, aula estudada”. Com esse lema, em nossa escola, o hábito do estudo diário é incorporado de forma natural. Além disso, o trabalho com textos em todas as disciplinas, o estímulo à interpretação textual e à pesquisa têm importância especial para a formação dos nossos estudantes. Visitas culturais e eventos comemorativos complementam e estimulam o conhecimento e aprimoram o convívio social.</p>

      </div>

      <div class=" foto-ensino-fund d-none d-lg-flex col-lg-6 px-0" style="background-image:url(<?= get_field('fundamental_i') ?>);"></div>

    </div>


    <div class="d-lg-flex pb-lg-5">
      <div class="foto-grid col-lg-7  pr-lg-5 px-0 ">

        <img class="img-anglo col-4 col-lg-3 px-0" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/anglo.png" alt="">

        <img src="<?= get_field('apostilas_fundamental_i'); ?>" alt="">

        <!-- <div class="foto-2 col-10 col-lg-7 px-0">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/foto1.png" alt="">

        </div> -->

      </div>

      <div class="texto-material pt-4 pl-lg-4 pt-lg-0 ">


        <span class="title px-3 px-lg-0"><b>Material Didático</b></span>

        <div class="line col-5"></div>

        <p>O planejamento na elaboração do material, do projeto gráfico ao grau de complexidade das atividades, permite que a transição entre as fases – dos anos iniciais aos finais – seja assimilada de forma organizada, gradual e espontânea.</p>

        <p>Nossos alunos recebem quatro cadernos anuais, nos quais o conteúdo é distribuído por blocos de aulas, e o número de aulas destinado a cada conteúdo leva em consideração a sua complexidade, a proposta dos autores e o planejamento da escola.</p>

        <div class="horario">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/relogio.png" alt="">

          <div>

            <span><b>Horário das aulas</b></span>
            <span>Das 7h10 às 11h40</span>

          </div>


        </div>

      </div>
    </div>
    <div class="texto-material pt-4 pl-lg-4 pt-lg-0 col-lg-6 ">


      <span class="title px-3 px-lg-0"><b>Plantão de dúvidas e Orientação de Estudos</b></span>

      <div class="line col-5"></div>

      <p>Ao fazerem a tarefa, é comum os alunos ficarem com dúvidas sobre determinados exercícios ou tópicos da matéria. Nos plantões de dúvida, eles poderão sanar suas dificuldades e reforçar os conceitos aprendidos em sala de aula.
        <br>Os plantões são semanais em dias e horários pré-determinados.
      </p>


      <!-- <div class="horario">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/relogio.png" alt="">

          <div>

            <span><b>Horário das aulas</b></span>
            <span>Das 7h10 às 11h40</span>

          </div>


        </div> -->

    </div>
  </div>
</section>

<section class="texto-intro blue-container">
  <div class="container">

    <div class="d-lg-flex pb-lg-5">
      <div class="texto">

        <span class="pre-title">Fundamental II - do 6º ao 9º ano</span>

        <span class="title px-5 px-lg-0">Aprofundar-se e consolidar o <b>aprendizado adquirido anteriormente é fundamental</b></span>

        <div class="line col-5"></div>

        <p>No Ensino Fundamental II do Colégio Méson, o hábito de estudo, bem como a diversidade das estratégias pedagógicas e das atividades propostas, têm como finalidade fazer com que os alunos aprofundem e consolidem os conhecimentos adquiridos anteriormente, sempre levando em conta a aproximação dos conteúdos ministrados não apenas com o cotidiano escolar, mas também com a realidade do mundo que nos cerca.</p>

        <p>A ênfase no processo de aquisição e desenvolvimento do hábito de estudos diário, por meio das tarefas sistematicamente apresentadas ao final de cada aula ou bloco de aulas, as propostas de aprofundamento e de pesquisa e a preparação gradual para fazer a transição entre os anos iniciais e os finais do Ensino Fundamental dão contornos a uma metodologia eficiente e ajustada às várias faixas etárias desse segmento. Nossos alunos vão sendo preparados pouco a pouco para que, no futuro, as possibilidades de sucesso sejam ainda mais amplas.</p>
      </div>

      <div class=" foto-ensino-fund-2 d-none d-lg-flex col-lg-6 px-0" style="background-image:url(<?= get_field('fundamental_ii') ?>);"></div>


    </div>


    <div class="d-lg-flex pb-lg-5">

      <div class="foto-grid col-lg-6  d-lg-flex   px-0 ">


        <img src="<?= get_field('apostilas_fundamental_ii'); ?>" alt="">

        <!-- <div class="foto-2 col-10 col-lg-7 p-0">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/foto1.png" alt="">

        </div> -->

      </div>

      <div class="texto-material pt-lg-0 pt-4 pl-lg-5 ml-lg-3">


        <span class="title px-3 px-lg-0"><b>Material Didático</b></span>

        <div class="line col-5"></div>

        <p>O planejamento na elaboração do material, do projeto gráfico ao grau de complexidade das atividades, permite que a transição entre as fases – dos anos iniciais aos finais – seja assimilada de forma organizada, gradual e espontânea. Nossos alunos recebem quatro cadernos anuais, nos quais o conteúdo é distribuído por blocos de aulas, e o número de aulas destinado a cada conteúdo leva em consideração a sua complexidade, a proposta dos autores e o planejamento da escola.</p>

        <div class="horario">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/relogio.png" alt="">

          <div>

            <span><b>Horário das aulas</b></span>
            <span>Das 7h10 às 11h50</span>

          </div>


        </div>

      </div>
    </div>
    <div class="texto-material pt-4 pl-lg-4 pt-lg-0 col-lg-6 ">


      <span class="title px-3 px-lg-0"><b>Plantão de dúvidas e Orientação de Estudos</b></span>

      <div class="line col-5"></div>

      <p>Ao fazerem a tarefa, é comum os alunos ficarem com dúvidas sobre determinados exercícios ou tópicos da matéria. Nos plantões de dúvida, eles poderão sanar suas dificuldades e reforçar os conceitos aprendidos em sala de aula.
        <br>Os plantões são semanais em dias e horários pré-determinados.
      </p>


      <!-- <div class="horario">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/relogio.png" alt="">

          <div>

            <span><b>Horário das aulas</b></span>
            <span>Das 7h10 às 11h40</span>

          </div>


        </div> -->

    </div>
  </div>
</section>


<?= get_footer(); ?>