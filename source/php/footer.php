<footer class="">

  <div class="container padding-0 d-lg-flex justify-content-between">
    <div class="col-lg-8 col-12 px-0">
      <div class="testa px-lg-0">

        <span class="title text-lg-left d-lg-block col-lg-7 px-0">ENTRE EM CONTATO </br>COM A GENTE</span>

      </div>

      <div class="form container  px-lg-0">

        <span><b>Funcionamento da Secretaria:</b></span>

        <span>De segunda a sexta-feira das 08h às 18h.</span>
        <span>Sábado das 08h às 12h.</span>

        <div class="line col-7 col-lg-4"></div>

        <?= do_shortcode('[contact-form-7 id="47" title="Formulário de contato footer"]'); ?>

        <!-- <label for="name">Nome</label>
        <input class="col-12 col-lg-9" type="text" name="name" id="" placeholder="Ex: Carlos Almeida">

        <label for="email">E-mail</label>
        <input class="col-12 col-lg-9" type="text" name="email" id="" placeholder="Ex: carlosalmeida@gmail.com">

        <label for="assunto">Assunto</label>
        <input class="col-12 col-lg-9" type="text" name="assunto" placeholder="Ex: Dúvidas - Inscrição">

        <label for="mensagem">Mensagem</label>
        <textarea class="col-12 col-lg-9" name="mensagem" id="" cols="30" rows="10"></textarea>

        <input class="" type="submit" value="Enviar mensagem"> -->

      </div>


    </div>

    <a href="https://www.google.com/maps/dir//Col%C3%A9gio+M%C3%A9son+-+R.+Dezesseis+de+Dezembro,+279+-+Centro,+Sumar%C3%A9+-+SP,+13170-018/@-22.8212198,-47.2701761,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x94c8bd6a2e951beb:0x55838cb332ecb48e!2m2!1d-47.2703489!2d-22.8210628" target="_blank" class="d-none d-lg-block mapa-lg">
      <!-- <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/map-lg.png" alt=""> -->
    </a>

  </div>

  <a href="https://www.google.com/maps/dir//Col%C3%A9gio+M%C3%A9son+-+R.+Dezesseis+de+Dezembro,+279+-+Centro,+Sumar%C3%A9+-+SP,+13170-018/@-22.8212198,-47.2701761,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x94c8bd6a2e951beb:0x55838cb332ecb48e!2m2!1d-47.2703489!2d-22.8210628" target="_blank" class="d-lg-none d-block map-sm"></a>

  <div class="footer">

    <div class="container">

      <div class="d-lg-flex">
        <div class="col-lg-3 text-md-center px-lg-0">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-footer.png" alt="">

          <span class="sub">Méson, entre os colégios com as maiores notas no ENEM da região.</span>
        </div>


        <div class="fale-conosco col-lg-9  d-lg-flex  justify-content-between px-0 pl-lg-5">

          <div class="d-none d-lg-flex flex-column  col-lg-3 mr-lg-5">

            <span class="title text-lg-left">Veja também</span>

            <a class="pb-2  text-left" href="<?= get_site_url(); ?>/o-colegio-meson">O Colégio Méson</a>
            <a class="pb-2  text-left" href="<?= get_site_url(); ?>/sistema-anglo">Sistema Anglo</a>
            <a class="pb-2  text-left" href="<?= get_site_url(); ?>/ensino-fundamental">Ensino Fundamental</a>
            <a class="pb-2  text-left" href="<?= get_site_url(); ?>/ensino-medio">Ensino Médio</a>
          </div>



          <div class="col-lg-4 px-0 mx-lg-5">
            <span class="title ">Fale Conosco</span>

            <span><b>Funcionamento da Secretaria:</b></span>
            <span>De segunda a sexta-feira das 07h às 18h.</span>
            <span>Sábado das 08h às 12h.</span>

            <div class="tel-email col-9 col-md-4 col-lg-12 px-0 ">
              <div class="telefones">

                <i class="fas fa-phone-alt"></i>
                <a href="tel:+551938831550"><span class="mini">19</span> 3883-1550</a>

              </div>

              <a href="mailto:atendimento@meson.com.br" class="email-link">
                <i class="fas fa-envelope"></i>
                atendimento@meson.com.br
              </a>

            </div>
          </div>

          <div class="col-lg-3 ml-lg-5 px-0 pr-lg-0">
            <div class="box-area-aluno">
              <span class="title">Área do Aluno</span>

              <a href="#" class="btn-login col-3 col-md-2  col-lg-5 ">
                <span>Login</span>
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/login.png" alt="">
              </a>

            </div>

            <span class="title pt-lg-4 d-none d-lg-block">Redes Sociais</span>

            <div class="midias pt-lg-0 col-4 px-0">

              <a href="https://www.facebook.com/cursocolegiomeson"><i class="fab fa-facebook-f"></i></a>
              <a href="https://www.instagram.com/mesoncursocolegio/"><i class="fab fa-instagram"></i></a>

            </div>
          </div>
        </div>
      </div>


      <div class="direitos-humann">
        <span>Todos os direitos reservados a <b>Colégio Méson</b> - 2020.</span>
        <span>Desenvolvido por <a href="https://humann.com.br"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/humann-logo.png" alt=""></a></span>
      </div>


    </div>
  </div>
</footer>


<?php wp_footer(); ?>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.3/jquery.inputmask.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>

<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/lightbox.min.js"></script>



<?php if (is_page('sistema-anglo')) : ?>

  <script>
    var sliderMetodologia = tns({
      container: '.carousel-metodologia',
      slideBy: 'page',
      autoplay: false,
      loop: true,
      controls: false,
      nav: false,
      autoplayButtonOutput: false,
      gutter: 10,
      responsive: {
        300: {
          items: 1.1,
        },
        768: {
          items: 2.1,
        },
        1200: {
          items: 4,
          loop: false
        },
      }
    });
  </script>

<?php endif; ?>

<?php if (is_page('o-colegio-meson')) : ?>

  <script>
    var sliderEstruturaSobre = tns({
      container: '.gallery-estrutura',
      mode: 'gallery',
      slideBy: 1,
      autoplay: false,
      loop: false,
      controls: false,
      navPosition: "bottom",
      autoplayButtonOutput: false,

    });
  </script>

<?php endif; ?>

<?php if (is_singular('evento')) : ?>

  <script>
    var sliderEvento = tns({
      container: '.carosel-galeria',
      items: 1,
      autoplay: false,
      loop: false,
      controls: false,
      autoplayButtonOutput: false,
      navPosition: 'bottom',
      responsive: {
        1024: {
          disable: true
        }
      }
    });
    var sliderEventosrela = tns({
      container: '.evento-home-mobile',
      items: 1,
      autoplay: false,
      loop: false,
      controls: false,
      autoplayButtonOutput: false,
      navPosition: 'bottom',
      gutter: 15,
      controlsPosition: 'bottom',
      responsive: {
        1024: {
          items: 3,
          nav: false,
          controls: true,
          controlsText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],

        }
      }
    });
  </script>

<?php endif; ?>
<?php if (is_page('eventos')) : ?>
  <script>
    var sliderEventosrela = tns({
      container: '.evento-home-mobile',
      items: 1,
      autoplay: false,
      loop: false,
      controls: false,
      autoplayButtonOutput: false,
      navPosition: 'bottom',
      gutter: 15,
      controlsPosition: 'bottom',
      responsive: {
        1024: {
          nav: false,
          controls: true,
          controlsText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],

        }
      }
    });
  </script>
<?php endif; ?>
<script>
  site = "<?= get_site_url(); ?>"
  stylesheet = "<?= get_stylesheet_directory_uri(); ?>"
</script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/js/app.js"></script>

</body>

</html>