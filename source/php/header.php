<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <!-- Google Tag Manager -->
  <script>
    (function(w, d, s, l, i) {
      w[l] = w[l] || [];
      w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
      });
      var f = d.getElementsByTagName(s)[0],
        j = d.createElement(s),
        dl = l != 'dataLayer' ? '&l=' + l : '';
      j.async = true;
      j.src =
        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
      f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-KMRKLSF');
  </script>
  <!-- End Google Tag Manager -->

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title> <?= wp_title(''); ?></title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">
  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/lightbox.min.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

</head>

<body>


  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KMRKLSF" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->


  <?php if (is_page('home')) : $b_home = 'banner-home';
    $b_home_lg = 'bh-lg';
    $d_lg_none = 'd-lg-none';

  else : $d_lg_none = ' ';
  endif ?>


  <div class="invisivel "></div>




  <header class="d-lg-block d-none  ">

    <div class="pre-header">
      <div class="container">
        <i class="fas fa-phone-alt"></i>
        <a href="tel:+551938831550"><span class="mini">19</span> 3883-1550</a>


        <a href="mailto:atendimento@meson.com.br" class="email-link">
          <i class="fas fa-envelope"></i>
          atendimento@meson.com.br
        </a>


        <a href="https://www.facebook.com/cursocolegiomeson"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.instagram.com/mesoncursocolegio/"><i class="fab fa-instagram"></i></a>

        <div class="col-lg-3 px-0 box-btn-header">
          <a href="https://apps.gennera.com.br/public/#/login" target='_blank' class="area-aluno col-lg-6 px-0">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado.png" alt="">
            <span>Área do Aluno</span>
          </a>

          <a href="https://apps.gennera.com.br/public/#/login" target="_blank" class="btn-login col-lg-4 ">
            <span>Login</span>
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/login.png" alt="">
          </a>
        </div>

      </div>
    </div>

    <div class="header <?= $b_home_lg; ?>">
      <div class="container">

        <a href="<?= get_site_url(); ?>/">
          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="">
        </a>

        <div class="links  px-0">

          <a href="<?= get_site_url(); ?>/o-colegio-meson">O Colégio Méson</a>
          <a href="<?= get_site_url(); ?>/sistema-anglo">Sistema Anglo</a>
          <a href="<?= get_site_url(); ?>/ensino-fundamental">Ensino Fundamental</a>
          <a href="<?= get_site_url(); ?>/ensino-medio">Ensino Médio</a>
          <a href="<?= get_site_url(); ?>/eventos">Eventos</a>
          <a href="<?= get_site_url(); ?>/aprovacoes-em-vestibulares">Alunos Aprovados</a>


        </div>


      </div>
    </div>



  </header>


  <nav class=" d-lg-none top-nav  " id="top-nav">

    <input class="menu-btn" type="checkbox" id="menu-btn" />

    <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label>

    <div class="box-midias col-3 col-md-1 px-0 mr-md-3">
      <a href="https://www.facebook.com/cursocolegiomeson"><i class="fab fa-facebook-f"></i></a>
      <a href="https://www.instagram.com/mesoncursocolegio/"><i class="fab fa-instagram"></i></a>
    </div>

    <div class="col-9 col-md-7 px-md-0 box-btn-header">
      <a href="https://apps.gennera.com.br/public/#/login" target="_blank" class="area-aluno col-7 px-0">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado.png" alt="">
        <span>Área do Aluno</span>
      </a>

      <a href="https://apps.gennera.com.br/public/#/login" target="_blank" class="btn-login col-4 col-md-2 ">
        <span>Login</span>
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/login.png" alt="">
      </a>
    </div>



    <div class="menu">

      <a href="<?= get_site_url(); ?>/o-colegio-meson">O Colégio Méson</a>
      <a href="<?= get_site_url(); ?>/sistema-anglo">Sistema Anglo</a>
      <a href="<?= get_site_url(); ?>/ensino-fundamental">Ensino Fundamental</a>
      <a href="<?= get_site_url(); ?>/ensino-medio">Ensino Médio</a>
      <a href="<?= get_site_url(); ?>/eventos">Eventos</a>
      <a href="<?= get_site_url(); ?>/aprovacoes-em-vestibulares">Alunos Aprovados</a>

    </div>


    <div class="col-11 box-btn-header-2 pr-0 ">
      <a href="https://apps.gennera.com.br/public/#/login" target="_blank" class="area-aluno col-6 px-0">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadeado.png" alt="">
        <span>Área do Aluno</span>
      </a>

      <a href="https://apps.gennera.com.br/public/#/login" target="_blank" class="btn-login col-3 ">
        <span>Login</span>
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/login.png" alt="">
      </a>
    </div>

  </nav>

  <?php if (is_page('ensino-medio')) : $px_5 = 'px-5';
  endif; ?>

  <div class="banner <?php echo $b_home; ?>">

    <a href="<?= get_site_url(); ?>" class="shadow d-lg-none">
      <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo-header.png" alt="">
    </a>

    <div class="page-name">
      <span class="title <?= $px_5; ?>"><?php echo  the_title(); ?></span>
    </div>

  </div>