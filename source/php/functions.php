<?php


function my_function_admin_bar()
{
	return false;
}
add_filter('show_admin_bar', 'my_function_admin_bar');

function cptui_register_my_cpts_aluno()
{

	/**
	 * Post Type: Aluno.
	 */

	$labels = [
		"name" => __("Alunos", "custom-post-type-ui"),
		"singular_name" => __("Aluno", "custom-post-type-ui"),
	];

	$args = [
		"label" => __("Aluno", "custom-post-type-ui"),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => ["slug" => "aluno", "with_front" => true],
		"query_var" => true,
		// "menu_icon" => "http://localhost/diligen/wp-content/uploads/2020/12/oie_transparent-1.png",
		"supports" => ["title", "excerpt", "custom-fields", "thumbnail" ],
	];

	register_post_type("aluno", $args);
}

add_action('init', 'cptui_register_my_cpts_aluno');
function cptui_register_my_cpts_evento() {

	/**
	 * Post Type: Eventos.
	 */

	$labels = [
		"name" => __( "Eventos", "custom-post-type-ui" ),
		"singular_name" => __( "Evento", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Eventos", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "evento", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title","excerpt", "editor", "thumbnail" ],
	];

	register_post_type( "evento", $args );
}

add_action( 'init', 'cptui_register_my_cpts_evento' );

add_theme_support( 'post-thumbnails' );

// Filter except length to 15 words.
// tn custom excerpt length
function tn_custom_excerpt_length( $length ) {
	return 10;
	}
	add_filter( 'excerpt_length', 'tn_custom_excerpt_length', 999 );
	function cptui_register_my_taxes_ano_aluno() {

		/**
		 * Taxonomy: Ano.
		 */
	
		$labels = [
			"name" => __( "Ano", "custom-post-type-ui" ),
			"singular_name" => __( "Ano", "custom-post-type-ui" ),
		];
	
		
		$args = [
			"label" => __( "Ano", "custom-post-type-ui" ),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => true,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => [ 'slug' => 'ano_aluno', 'with_front' => true, ],
			"show_admin_column" => false,
			"show_in_rest" => true,
			"rest_base" => "ano_aluno",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => true,
			"show_in_graphql" => false,
		];
		register_taxonomy( "ano_aluno", [ "aluno" ], $args );
	}
	add_action( 'init', 'cptui_register_my_taxes_ano_aluno' );
	
?>