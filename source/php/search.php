<?php get_header(  ) ?>
<?php
        wp_reset_query();
          $argsListar = array(
            'post_type' => 'evento',
            'posts_per_page' => 9,
            's' => $s
          );
          $eventoListar = new WP_Query($argsListar);
        //   if($eventoListar->have_posts()): while($eventoListar->have_posts(  )): $eventoListar->the_post();
?>
    <?php get_search_form() ?>
    <h2 class="resultado">Sua busca por "<?= $s ?>" gerou <?= $eventoListar->found_posts ?> resultados</h2>
    <div class="list-eventos container wrapper-eventos-home">
      <div class="row evento-home-mobile" id="app-eventos">
      <?php
      
          if($eventoListar->have_posts()): while($eventoListar->have_posts(  )): $eventoListar->the_post();
        ?>
        <div class="col-lg-4 evento-listado">
          <div class="imagem-destaque">
            <?php the_post_thumbnail() ?>
          </div>
          <div class="infos">
                <div class="data"><i class="far fa-calendar-alt"></i> <?= get_field('data') ?></div>
                <h2><?php the_title(); ?></h2>
                <a href="<?php the_permalink(); ?>">Veja todas as fotos</a>
            </div>
        </div>
        <?php endwhile; endif; ?>
      </div>
    </div>
    <div class="carregar-mais">
      <button id="get-evento" data-page="1" data-busca="<?php echo $s ?>">Carregar mais...</button>
    </div>
<?php get_footer(  ) ?>