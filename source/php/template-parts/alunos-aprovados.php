<div class="wrapper container wrapper-alunos">

    <div class="carousel-alunos alunos">
        <?php
        $args_alunos = array(
            'post_type' => 'aluno',
            'posts_per_page' => 10,
            "order" => 'ASC',
            'orderby' => 'title'

        );
        $alunos = new WP_Query($args_alunos);
        if ($alunos->have_posts()) : while ($alunos->have_posts()) : $alunos->the_post();
        ?>
                <div class="aluno">
                    <div class="foto-aluno" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">
                        <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/mascara-aprovados.png" alt="">
                    </div>
                    <div class="info-aluno">
                        <h3 class="nome"><?php the_title() ?></h3>

                        <?php if (have_rows('curso_instituicao')) : ?>
                            <?php while (have_rows('curso_instituicao')) : the_row(); ?>

                                <p class='mb-1 curso'><?php echo the_sub_field('curso'); ?> - <?php echo the_sub_field('instituicao'); ?></p>
                            <?php endwhile; ?>
                        <?php endif; ?>

                        <!-- <p class="curso"><?php the_field('curso') ?></p>
                        <img src="<?php the_field('logo_universidade') ?>" alt="<?php the_field('nome_universidade') ?>"> -->
                    </div>
                </div>
        <?php
            endwhile;
        endif;
        ?>
    </div>
    <div class="text-center mt-4">
        <a href="<?= get_site_url() ?>/aprovacoes-em-vestibulares" class="btn-cta cta-alunos col-lg-3 col-8 px-0"> Veja todos os aprovados</a>
    </div>
</div>