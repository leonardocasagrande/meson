<?php
// Template Name: Alunos
get_header(); ?>

<!-- <section class="texto-intro ">
  <div class="container">

    <div class="texto">


      <span class="title px-3 mb-3">+ de 200 <b>aprovados</b><br>em 2020</span>

      <p class="px-3">Ano a ano, reafirmamos nosso compromisso com o futuro e a carreira de centenas de jovens. No próximo, um deles pode ser o seu filho!</p>
      <div class="line col-5 col-lg-12"></div>


    </div>

    

  </div>
</section> -->


<section class="wrapper-alunos container">
  <!-- <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="todos-tab" data-toggle="tab" href="#todos" role="tab" aria-controls="todos" aria-selected="true">todos</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="Ano2020-tab" data-toggle="tab" href="#Ano2020" role="tab" aria-controls="2020" aria-selected="false">2020</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="Ano2019-tab" data-toggle="tab" href="#Ano2019" role="tab" aria-controls="2019" aria-selected="false">2019</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="Ano2018-tab" data-toggle="tab" href="#Ano2018" role="tab" aria-controls="2018" aria-selected="false">2018</a>
    </li>
  </ul> -->
  <div class="tab-content mt-5" id="nav-tabContent">
    <div class="tab-pane fade show active" id="todos" role="tabpanel" aria-labelledby="tAnoodos-tab">
        <div class="row">
          <?php
          $paged_all = (isset($_GET['pagina']) ? $_GET['pagina'] : 1);
          $argsAlunoAll = array(
            'post_type' => 'aluno',
            'posts_per_page' => 8,
            'orderby' =>'title',
            'order' => 'ASC',
            'paged' => $paged_all,
          );
          $alunos_all = new WP_Query($argsAlunoAll);

          if ($alunos_all->have_posts()) : while ($alunos_all->have_posts()) : $alunos_all->the_post();

          ?>
              <div class="col-md-6 col-lg-3 px-0 px-lg-3 alunos">
                <div class="aluno ">
                  <div class="foto-aluno" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/mascara-aprovados.png" alt="">
                  </div>
                  <div class="info-aluno">
                    <h3 class="nome"><?php the_title() ?></h3>

                    <?php if (have_rows('curso_instituicao')) : ?>
                      <?php while (have_rows('curso_instituicao')) : the_row(); ?>

                        <p class='mb-1'><?php echo the_sub_field('curso'); ?> - <?php echo the_sub_field('instituicao'); ?></p>
                      <?php endwhile; ?>
                    <?php endif; ?>
                    <!-- <p class="curso"><?php the_field('curso') ?></p>
                    <img src="<?php the_field('logo_universidade') ?>" alt="<?php the_field('nome_universidade') ?>"> -->
                  </div>
                </div>
              </div>


          <?php endwhile;
          endif; ?>
        </div>
        <!-- <div class="carregar-mais">
          <button id="get-aluno" data-page="1">Carregar mais...</button>
        </div> -->

        <div class="barradenavegacao">
          <?php


          echo paginate_links(array(
            'format' =>
            '?pagina=%#%#anchor', 'show_all' => false, 'current' => max(1, $paged_all), 'total' => $alunos_all->max_num_pages, 'prev_text' => '<i class="fas fa-caret-left fa-2x"></i>', 'next_text' => '<i class="fas fa-caret-right fa-2x"></i>',
            'type' => 'list'
          ));
          ?>
        </div>
    </div>
    <!-- <div class="tab-pane fade" id="Ano2020" role="tabpanel" aria-labelledby="Ano2020-tab">
          <div class="row">
          <?php
          $argsAluno2020 = array(
            'post_type' => 'aluno',
            'posts_per_page' => -1,
            'orderby'=>'title',
            'order' => 'ASC',
            'tax_query' => array(
              array(
                  'taxonomy' => 'ano_aluno',
                  'field'    => 'slug',
                  'terms'    => '2020',
              ),
            ),
          );
          $alunos2020 = new WP_Query($argsAluno2020);

          if ($alunos2020->have_posts()) : while ($alunos2020->have_posts()) : $alunos2020->the_post();

          ?>
              <div class="col-md-6 col-lg-3 px-0 px-lg-3 alunos">
                <div class="aluno ">
                  <div class="foto-aluno" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/mascara-aprovados.png" alt="">
                  </div>
                  <div class="info-aluno">
                    <h3 class="nome"><?php the_title() ?></h3>

                    <?php if (have_rows('curso_instituicao')) : ?>
                      <?php while (have_rows('curso_instituicao')) : the_row(); ?>

                        <p class='mb-1'><?php echo the_sub_field('curso'); ?> - <?php echo the_sub_field('instituicao'); ?></p>
                      <?php endwhile; ?>
                    <?php endif; ?>
                    <p class="curso"><?php the_field('curso') ?></p>
                    <img src="<?php the_field('logo_universidade') ?>" alt="<?php the_field('nome_universidade') ?>"> 
                  </div>
                </div>
              </div>


          <?php endwhile;
          endif; ?>
        </div>
         <div class="carregar-mais">
          <button id="get-aluno" data-page="1">Carregar mais...</button>
        </div> 

        
    </div>
    <div class="tab-pane fade" id="Ano2019" role="tabpanel" aria-labelledby="Ano2019-tab">
      <div class="row">
      <?php
      $argsAluno2019 = array(
        'post_type' => 'aluno',
        'posts_per_page' => -1,
        'order' => 'ASC',
        'tax_query' => array(
          array(
              'taxonomy' => 'ano_aluno',
              'field'    => 'slug',
              'terms'    => '2019',
          ),
        ),
      );
      $alunos2019 = new WP_Query($argsAluno2019);

      if ($alunos2019->have_posts()) : while ($alunos2019->have_posts()) : $alunos2019->the_post();

      ?>
          <div class="col-md-6 col-lg-3 px-0 px-lg-3 alunos">
            <div class="aluno ">
              <div class="foto-aluno" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/mascara-aprovados.png" alt="">
              </div>
              <div class="info-aluno">
                <h3 class="nome"><?php the_title() ?></h3>

                <?php if (have_rows('curso_instituicao')) : ?>
                  <?php while (have_rows('curso_instituicao')) : the_row(); ?>

                    <p class='mb-1'><?php echo the_sub_field('curso'); ?> - <?php echo the_sub_field('instituicao'); ?></p>
                  <?php endwhile; ?>
                <?php endif; ?>
                 <p class="curso"><?php the_field('curso') ?></p>
                <img src="<?php the_field('logo_universidade') ?>" alt="<?php the_field('nome_universidade') ?>"> 
              </div>
            </div>
          </div>


      <?php endwhile;
      endif; ?>
    </div>
    </div>
    <div class="tab-pane fade" id="Ano2018" role="tabpanel" aria-labelledby="Ano2018-tab">
          <div class="row">
          <?php
          $argsAluno2018 = array(
            'post_type' => 'aluno',
            'posts_per_page' => -1,
            'order' => 'ASC',
            'tax_query' => array(
              array(
                  'taxonomy' => 'ano_aluno',
                  'field'    => 'slug',
                  'terms'    => '2018',
              ),
            ),
          );
          $alunos2018 = new WP_Query($argsAluno2018);

          if ($alunos2018->have_posts()) : while ($alunos2018->have_posts()) : $alunos2018->the_post();

          ?>
              <div class="col-md-6 col-lg-3 px-0 px-lg-3 alunos">
                <div class="aluno ">
                  <div class="foto-aluno" style="background-image:url(<?= get_the_post_thumbnail_url() ?>)">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/dist/img/mascara-aprovados.png" alt="">
                  </div>
                  <div class="info-aluno">
                    <h3 class="nome"><?php the_title() ?></h3>

                    <?php if (have_rows('curso_instituicao')) : ?>
                      <?php while (have_rows('curso_instituicao')) : the_row(); ?>

                        <p class='mb-1'><?php echo the_sub_field('curso'); ?> - <?php echo the_sub_field('instituicao'); ?></p>
                      <?php endwhile; ?>
                    <?php endif; ?>
                     <p class="curso"><?php the_field('curso') ?></p>
                    <img src="<?php the_field('logo_universidade') ?>" alt="<?php the_field('nome_universidade') ?>"> 
                  </div>
                </div>
              </div>


          <?php endwhile;
          endif; ?>
        </div>
        
    </div> -->
  </div>
  
</section>
<?php get_footer(); ?>