<?= get_header(); ?>

<section class="texto-intro page-sistema-anglo">
  <div class="container d-lg-flex">

    <div class="texto col-lg-6 px-0">

      <span class="title col-lg-8 px-0">Somos pioneiros no <b>Sistema Anglo</b> de <b>Ensino em Sumaré</b></span>

      <div class="line col-5"></div>

      <p>Nossa proposta pedagógica sempre teve o propósito de criar uma escola que prepara para a vida. Aqui no Méson, queremos formar cidadãos atuantes, capazes de reconhecer as diferentes situações em que estão inseridos, compreender a extensão, a profundidade e a relação dos conhecimentos tratados nas diferentes áreas de estudo a fim de desenvolver uma consciência crítica da realidade, posicionando-se e atuando lucidamente frente aos desafios do caminho.</p>

      <p>Para isso, adotamos o Sistema Anglo de ensino, com material forte e em dia com as novas tecnologias. Um exemplo disso é o aplicativo Plurall, que faz parte do material didático do Ensino Fundamental II, Ensino Médio e Pré-Vestibular. Por meio dessa ferramenta, os alunos podem fazer suas tarefas, assistir a vídeos explicativos, reler textos que trabalham os assuntos de aula de modo aprofundado e, ainda, tirar dúvidas com plantonistas online.
      </p>


    </div>

    <div class="foto-grid">

      <img class="img-anglo col-4 px-0 " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/anglo.png" alt="">

      <img class="foto-1" src="<?= get_field('sala_de_aula') ?>" alt="">

      <div class="foto-2  px-0">

        <img src="<?= get_field('material_escolar'); ?>" alt="">

      </div>

    </div>

  </div>
</section>

<section class="metodologia">

  <div class="container">

    <p>Primeira Solução Educacional do mundo, desde 1950, o Sistema Anglo de Ensino visa potencializar a prática em sala de aula com qualidade superior, metodologia diferenciada e alinhada ao que há de mais avançado na educação. Ou seja, essa parceria proporciona a evolução sistema de ensino do nosso colégio, unindo: </p>

    <div class="wrapper">

      <div class="carousel-metodologia">

        <div>
          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/met1.png" alt="">
            <span class="text">Metodologia eficiente: <b>“Aula dada, aula estudada, hoje!”</b>.</span>

          </div>
        </div>


        <div>
          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/met2.png" alt="">
            <span class="text"><b>Informações detalhadas</b> para diagnosticar demandas pedagógicas específicas.</span>

          </div>
        </div>

        <div>
          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/met3.png" alt="">
            <span class="text">Inclusão efetiva de recursos <b>tecnológicos inovadores</b> no desenvolvimento dos alunos.</span>

          </div>
        </div>

        <div>
          <div class="item">

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/met4.png" alt="">
            <span class="text"><b>Apoio pedagógico contínuo</b> e estruturado, para construção conjunta de planos de ação</span>

          </div>
        </div>

      </div>

    </div>

  </div>


</section>

<section class="sistema-anglo">

  <div class="container">

    <img class="img-anglo col-md-2 px-0 " src="<?= get_stylesheet_directory_uri(); ?>/dist/img/anglo-box.png" alt="">

    <div class="itens col-lg-9 px-0">
      <div class="col-7 col-lg-2 px-0 item">

        <span class="text"><b>70 anos</b> de tradição e sucesso</span>
        <div class="line"></div>

      </div>

      <div class="line d-none d-lg-flex"></div>


      <div class="col-7 col-lg-3 px-0 item">

        <span class="text"><b>800 escolas</b> credenciadas</span>
        <div class="line"></div>

      </div>

      <div class="line d-none d-lg-flex"></div>


      <div class="col-7 col-lg-2 px-0 item">

        <span class="text"><b>+ 300 mil alunos </b>em todo Brasil</span>
        <div class="line"></div>

      </div>

      <div class="line d-none d-lg-flex"></div>


      <div class="col-7 col-lg-3 px-0 item">

        <span class="text"><b>Líder de aprovação</b> em universidades</span>
        <div class="line"></div>

      </div>
    </div>
  </div>

</section>


<?= get_footer(); ?>