<?= get_header(); ?>

<section class="texto-intro page-colegio-meson">
  <div class="container">

    <div class="texto col-lg-6 px-0  ">

      <span class="title pt-lg-3 pr-lg-5 mr-lg-5 ">A nossa <b>paixão</b> pelo conhecimento começa <b>pelo nome</b></span>

      <div class="line col-5"></div>

      <p>Nosso colégio foi fundado em 1998 e recebeu o nome Méson, em homenagem ao professor e físico brasileiro César Lattes, que teve uma importante participação na descoberta da partícula subatômica “Méson-pi”, sua maior contribuição para a área.</p>

      <p>No início, tínhamos apenas o Ensino Médio e já no primeiro ano obtivemos aprovações nos vestibulares mais concorridos da época. A partir desse excelente resultado, expandimos nossos cursos a séries do Ensino Fundamental I e II, até completarmos todo o ciclo de uma educação básica de qualidade.</p>


    </div>

    <div class="foto-grid px-0 col-lg-6 ">

      <img class="img-anglo col-4 px-0 col-lg-4 px-lg-4 mb-4 mb-lg-0 mx-auto mx-lg-0 d-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/anglo.png" alt="">

      <!-- <div class="foto-1 col-12 px-0"></div>

      <div class="foto-2 col-10 col-lg-8"></div>

      <div class="foto-3 col-lg-7 d-lg-block d-none"></div> -->

      <div class="position-texto">
        <p>Hoje, com mais de 20 anos de história, acreditamos que, além de ser a extensão de casa e de preparar bons estudantes, a escola tem a função de se juntar à família na formação de cidadãos conscientes e críticos que possam transformar o mundo em que vivemos em um lugar melhor. Para isso, buscamos promover um ambiente saudável onde estudantes, professores e coordenadores convivam de modo dinâmico, harmônico, próximo e respeitoso.
        </p>


      </div>
    </div>

  </div>
</section>

<!-- <section class="conheca-equipe">

  <div class="testa">

    <span class="title">Conheça nossa equipe</span>

  </div>

  <div class="conteudo container">

    <p> A experiência acumulada pela direção, coordenação e corpo docente aponta os caminhos futuros, em que pretendemos aliar o ensino tradicional ao que há de melhor dentro de cada uma das novas teorias pedagógicas. Somamos experiência, conhecimento e paixão pela educação para continuar fazendo do nosso colégio a referência em educação na cidade de Sumaré.</p>

    <div class="wrapper">

      <div class="carousel-equipe">

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/car-item.png" alt="">
          <span class="name">Cláudia Rosa</span>
          <span class="funcao">Diretora pedagógica</span>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/car-item.png" alt="">
          <span class="name">Cláudia Rosa 2</span>
          <span class="funcao">pedagógica</span>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/car-item.png" alt="">
          <span class="name">Cláudia Rosa</span>
          <span class="funcao">Diretora pedagógica</span>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/car-item.png" alt="">
          <span class="name">Cláudia Rosa 2</span>
          <span class="funcao">pedagógica</span>

        </div>

        <div class="item">

          <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/car-item.png" alt="">
          <span class="name">Cláudia Rosa 2</span>
          <span class="funcao">pedagógica</span>

        </div>

      </div>

    </div>

  </div>

</section> -->
<?php $galeria_fotos = get_field('fotos', 41);
if ($galeria_fotos) : ?>
  <section class="estrutura">

    <div class="head">

      <span class="title">Conheça nossa estrutura</span>

      <div class="line d-lg-none"></div>

    </div>

    <?= get_template_part('carousel-estrutura'); ?>


    <!-- <div class="wrapper-gallery container d-none d-lg-block">

    <div class="gallery-estrutura">
    <?php
    $controler_layout = 1;
    $controler_numero = 1;
    $total = count($galeria_fotos);
    foreach ($galeria_fotos as $foto) :
      // echo $controler_layout;
      if ($controler_layout == 1) :
    ?>
      <div class="item">
        <div class="d-flex justify-content-between">

          <img class="px-2 col-5" src="<?= esc_url($foto['url']) ?>" alt="">
          
        <?php elseif ($controler_layout == 2) : ?>
          <div class="px-0 col-7  d-flex flex-column justify-content-center ">
            <img style="max-width: 100%" class="px-0 pb-2 col-6" src="<?= esc_url($foto['url']) ?>" alt="">
            

            <div class="d-flex ">
            <?php elseif ($controler_layout == 3) : ?>
              <img class="px-0 pr-1 col-6" src="<?= esc_url($foto['url']) ?>" alt="">
              <?php elseif ($controler_layout == 4) : $controler_layout = 0; ?>
                <img class="px-0 pr-1 col-6" src="<?= esc_url($foto['url']) ?>" alt="">
           
            </div>

          </div>
        </div>
      </div>
      <?php endif;
            $controler_layout++;
          endforeach; ?>
      

    </div>
  
  </div>
  </div> -->

  </section>
<?php endif; ?>

<section class="unidades">

  <div class="testa">

    <span class="title">Excelência em <b>aprovação</b> nas melhores universidades</span>

  </div>

  <div class="faculdades container">

    <p>Ano a ano, reafirmamos nosso compromisso com o futuro e a carreira de centenas de jovens. </br>No próximo, um deles pode ser o seu filho ou a sua filha!</p>

    <div class="box-logos">

      <div class="col-lg-6 col-4 px-0 align-items-center ">
        <img style="width: 150px;" class="mb-5 mt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/USP.png" alt="">
        <img style="width: 140px;" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/PUC.png" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Unesp.png" alt="">
      </div>

      <div class="col-lg-6 col-4 px-0 align-items-center">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Unicamp.png" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/Facamp.png" alt="">
        <img style="width: 160px;" class="" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/FMJ.png" alt="">
      </div>

    </div>

  </div>

</section>




<?= get_footer(); ?>