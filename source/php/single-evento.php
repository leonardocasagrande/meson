<?php get_header(); the_post() ?>
<section class="single-evento container-lg p-0">
    <div class="cabecalho">
        <i class="far fa-calendar-alt"></i> <?= get_field('data') ?>
    </div>
    <div class="imagem-destaque">
        <?php the_post_thumbnail() ?>
    </div>
    <div class="conteudo">
        <?php the_content()  ?>
    </div>
    <div class="galeria">
    <?php 
        $images = get_field('galeria_de_fotos');
        $size = 'full'; // (thumbnail, medium, large, full or custom size)
        if( $images ): ?>
        <div class="carosel-galeria">
        <?php foreach( $images as $image_url ): ?>
            <div class="item col-lg-4 py-lg-3 px-0 px-lg-3">
                <div class="foto">
                <a href="<?= $image_url ?>" data-lightbox="galeira-evento"><img src="<?= $image_url ?>" alt=""></a>

                </div>
            </div>
        <?php endforeach; ?>
        </div>
    <?php endif; ?>
    </div>
    <div class="wrapper-eventos-home  container">
      <div class="evento-home-mobile">
        <?php
        wp_reset_query();
          $argsMobile = array(
            'post_type' => 'evento',
            'posts_per_page' => 6,
            'orderby'=> 'rand'
          );
          $eventoMobile = new WP_Query($argsMobile);
          if($eventoMobile->have_posts(  )): while($eventoMobile->have_posts(  )): $eventoMobile->the_post();
        ?>
        <div class="item-slide">
          <div class="img-destaque">
              <?php the_post_thumbnail() ?>
          </div>
          <div class="infos">
              <div class="data"><i class="far fa-calendar-alt"></i> <?= get_field('data') ?></div>
              <h2><?php the_title(); ?></h2>
              <p><?php the_excerpt(); ?></p>
              <a href="<?php the_permalink(); ?>">Veja todas as fotos</a>
          </div>
        </div>
        <?php endwhile; endif; ?>
      </div>
    </div>
    <!-- <div class="d-none d-lg-block evento-home-desk">
      <div class="carosel-eventos">
      <?php
      wp_reset_query();
        $argsDesktop = array(
          'post_type' => 'evento',
          'posts_per_page' => 3
        );
        $eventoDesktop = new WP_Query($argsDesktop);
        if($eventoDesktop->have_posts(  )): while($eventoDesktop->have_posts(  )): $eventoDesktop->the_post();
      ?>
        <div class="item">
          <div class="container">
            <div class="row">
              <div class="col-4 informacoes">
                <div class="data"><i class="far fa-calendar-alt"></i> <?= get_field('data') ?></div>
                <h2><?php the_title(); ?></h2>
                <p><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>">Veja todas as fotos</a>
              </div>
              <div class="col-6 imagem-destaque"><?php the_post_thumbnail() ?></div>
              <div class="col-2 galeria pl-0">
              <?php 
                $images = get_field('galeria_de_fotos');
                if( $images ): 
                // var_dump($images);
              ?>
                <div class="img-galeria ">
                  <img src="<?= $images[0] ?>" alt="">
                </div>
                <div class="img-galeria">
                  <img src="<?= $images[1] ?>" alt="">
                </div>
                <div class="img-galeria ">
                  <img src="<?= $images[2] ?>" alt="">
                </div>
              <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      <?php endwhile; endif; ?>

      </div>
    </div> -->
</section>
<?php get_footer(); ?>