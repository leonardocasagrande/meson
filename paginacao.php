<div class="barradenavegacao px-3"> <?php
    echo paginate_links( array(
        'format' => '?pagina=%#%',
        'show_all'  => true,
        'current' => max( 1, $paged ),
        'total' => $alunos->max_num_pages,
        'prev_text' => '<',
        'next_text' => '>',
        'type'      => 'list'
    ) );
    
    ?> </div>